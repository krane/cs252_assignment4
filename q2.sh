#!/bin/bash

awk '
BEGIN { FS=", "; }
{ print "define(NAME_OF_USER,"$1")dnl" > "m4defines";
  print "define(EMAIL_OF_USER,"$2")dnl" >> "m4defines";
  print "define(PAN_OF_USER,"$3")dnl" >> "m4defines";
  print "define(DONATION_OF_USER,"$4")dnl" >> "m4defines";
  system("m4 m4defines mail-template.m4 | msmtp -t " $2);
  close("m4defines");
}
END{}' input.csv

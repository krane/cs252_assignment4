#!/bin/bash

awk '
BEGIN { FS=", "; }
{ print "define(NAME_OF_USER,"$1")dnl" > "m4defines";
  print "define(EMAIL_OF_USER,"$2")dnl" >> "m4defines";
  print "define(PAN_OF_USER,"$3")dnl" >> "m4defines";
  print "define(DONATION_OF_USER,"$4")dnl" >> "m4defines";
	
  system("m4 m4defines mail-template.m4 > temp");
  print "\n." >> "temp"
  print "quit" >> "temp"
  
  print "ehlo smtp.cse.iitk.ac.in" > "mail"
  print "MAIL FROM: <krane@cse.iitk.ac.in>" >> "mail"
  print "RCPT TO: <krane@cse.iitk.ac.in>" >> "mail"
  #printf "RCPT TO: <%s>\n",$2 >> "mail"
  print "DATA" >> "mail"
  print "From: <krane@cse.iitk.ac.in>" >> "mail"
  printf "To: <%s>\n",$2 >> "mail"
  system("cat temp >> mail");
  system("sleep 1");
  system("nc smtp.cse.iitk.ac.in 25 < mail");
  close("m4defines");
  close("temp");
  close("mail");

}
END{}' input.csv




#!/bin/bash

cp $1 body

header1='ehlo smtp.cse.iitk.ac.in
MAIL FROM:<krane@cse.iitk.ac.in>
RCPT TO:<EMAIL_OF_USER>
DATA
From: <krane@cse.iitk.ac.in>
Content-Type: multipart/mixed; boundary="BOUNDARYTEXT"
Subject:80G LETTER
To: <EMAIL_OF_USER>

--BOUNDARYTEXT
Content-Transfer-Encoding: quoted-printable
Content-Type: text/plain;
'

header2='


--BOUNDARYTEXT
Content-Disposition: inline; filename=80g.pdf
Content-Type: application/pdf; x-unix-mode=0644; name="80g.pdf"
Content-Transfer-Encoding: base64
'

echo "$header1" > header;
cat $3 >> header;
echo "$header2" >> header;


awk '
BEGIN { FS=", "; }
{ print "define(NAME_OF_USER,"$1")dnl" > "m4defines";
  print "define(DONATION_OF_USER,"$4")dnl" >> "m4defines";
  print "define(EMAIL_OF_USER,"$2")dnl" >> "m4defines";
  print "define(PAN_OF_USER,"$3")dnl" >> "m4defines";
  system("cat m4defines > testpdf");
  print "\n" >> "testpdf"
  system("cat 80g.mail.m4 >> testpdf");
  system("m4 testpdf > pdf.tex");
  system("pdflatex pdf.tex");
  system("cat m4defines > template");
  print "\n" >> "template"
  system("cat header >> template");
  print "\n" >> "template"
  system("(cat pdf.pdf | base64) >> template");
  print "\n" >> "template"
  print "--BOUNDARYTEXT--\n." >> "template"
  system("m4 template > mail");
  system("nc smtp.cse.iitk.ac.in 25 < mail");
  close("m4defines");
  close("mail");
}
END{}' input.csv
